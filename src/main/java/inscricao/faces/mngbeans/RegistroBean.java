/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author lucas
 */
@ApplicationScoped
public class RegistroBean extends utfpr.faces.support.PageBean{
    
    ArrayList<inscricao.entity.Candidato> CandidatosList;
    
    public void start_list()
    {
        CandidatosList = new ArrayList<inscricao.entity.Candidato>();
    }
    
    public void add_to_list(inscricao.entity.Candidato candidato){
        CandidatosList.add(candidato);
    }
}
